# Calcfix - End of Development Notice:
Calcfix development has ended. While fun, I have gotten bored of coding for TI calculators, moved on to Python, and the program is really at the best state it can be at. Bug fixes or minor features might be applied in the near future.

# Welcome to Calcfix!

Calcfix (or Calculator Fix) has one simple mission:
Fix your graphing calculator before class!

If you've ever been in class, and your graphing is totally messed up, or someone messed with your settings, Calcfix can fix it. Whether it's from a classmate fooling with your window settings, or a program you ran that messed something up, this will fix it, without diving through the memory window, or having to reset your calculator entirely.

This program runs entirely automatically, with only 2 inputs to hit enter at the beginning, and for a garbage collect (on the all & garbage collect programs).

On most calculators, there will be the all program, and sub-programs that can fix a certain issue if you know what the issue is, so you don't have to go through the entire fix program, which can help cut time in fixing an issue.

All programs are tested before their release to make sure everything works, as reliability is the most important thing with this program.

In some cases, the all program can actually free up some space! (tested to free up around 6-7 KB on a TI-84 Plus CE with the early versions)

That's why Calcfix was made. To fix that dumb problem of things getting broken, and your teacher erasing your beautiful "I LOVE MATH" drawing in Cabri Jr.
